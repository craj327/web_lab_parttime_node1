var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);
app.get('/about', function (req, res) {
    res.type('text/html');
    //res.send('About Me');
    res.sendFile(__dirname + '/public/about.html');
});

app.use(express.static(__dirname + '/public'));

// 404 page
app.use(function (req, res) {
    res.type('text/html');
    res.status(404);
    res.send('Page Not Found');
});
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' +
        app.get('port'));
});
