
(function initPage() {
    // Get array containing all the pages
    // var pages = document.getElementsByClassName('page');
    var pages = document.querySelectorAll(".page");

    for (var i = 0; i < pages.length; i++) {
        // Remove all the delays from all the pages
        pages[i].style.animationDelay = "0s";

        // Set all the pages to have the onclick function
        pages[i].setAttribute("onclick", "pageClick(this)");

        // Add an event listener to change the z-index of each page after the current pages finishes it's animation
        pages[i].addEventListener("animationend", updateZ) ; 
    }
})();

function updateZ(element)
{
    var isElementFound = false;
    var pages = document.querySelectorAll(".page");

    for (var i = 0; i < pages.length; i++) {
        if(pages[i].id === element.target.id)
        {
            isElementFound = true;
            continue;
        }

        if(isElementFound)
        {
            pages[i].style.zIndex = i;
            break;
        }
    }
}

function pageClick(element){
    element.classList.add("pageAnimation");
}

