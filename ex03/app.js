var http = require('http');
var fs = require('fs');
var path = require('path');

http.createServer(function (req, res) {

    var lookup = path.basename(decodeURI(req.url));
    console.log (lookup);

    if (lookup == "about") {

        fs.readFile(__dirname + '/public/about.html', function (err, data) {

            if (err) {
                res.writeHead(500, { 'Content-Type': 'text/plain' });
                res.end('500 – Internal Error');
            } else {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.end(data);
            }
        });
    }

    else {
        var file = __dirname + "/public/images/" + lookup;
        fs.exists(file, function (exists) {
            if (exists) {
                fs.readFile(file, function (err, data) {
                    res.writeHead(200, { "Content-Type": "image/png" });
                    res.end(data);
                });
            }
        });
    }
}).listen(3000);